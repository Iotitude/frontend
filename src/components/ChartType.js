// libs
import React from 'react';
// components
import { MenuItem, SelectField } from 'material-ui';

export default class ChartType extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 'bar'
        }
        this.updateChartType = this.updateChartType.bind(this);
    }
    updateChartType(e, index, value) {
        this.setState({value});
        this.props.updateChartType(value);
    }
    /// !!! add <MenuItem /> with corresponding values if adding new type of chart
    render() {
        return(
            <SelectField
                id="chart-select"
                floatingLabelText="Chart"
                value={this.state.value}
                style={{maxWidth: '200px', marginLeft: '30px'}}
                onChange={this.updateChartType}>
                <MenuItem id="chart-select-bar" value={'bar'} primaryText="Bar" />
                <MenuItem id="chart-select-pie" value={'pie'} primaryText="Pie" />
                <MenuItem id="chart-select-doughnut" value={'doughnut'} primaryText="Doughnut" />
                <MenuItem id="chart-select-polar" value={'polar'} primaryText="Polar" />
            </SelectField>
        );
    }
}
