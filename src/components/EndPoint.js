// libs
import React from 'react';
// components
import { MenuItem, SelectField } from 'material-ui';

export default class EndPoint extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 'range?'
        }
        this.updateEndPoint = this.updateEndPoint.bind(this);
    }
    updateEndPoint(e, index, value) {
        this.setState({value});
        this.props.updateEndPoint(value);
    }
    /// !!! add <MenuItem /> with corresponding values if adding new endpoints
    /// !!! change primaryText for better names
    render() {
        return(
            <SelectField
                id="endpoint-select"
                floatingLabelText="Endpoint"
                floatingLabelFixed={true}
                style={{maxWidth: '200px', marginLeft: '30px'}}
                value={this.state.value}
                onChange={this.updateEndPoint}>
                <MenuItem id="enpoint-select-all" value={'range?'} primaryText="All" />
                <MenuItem id="enpoint-select-1" value={'endpointrange?endpoint=Nu4W6wHRhTBifmy64ld74EqDWF4%3D&'} primaryText="1" />
                <MenuItem id="enpoint-select-2" value={'endpointrange?endpoint=9JQgtPwRfHxVUhp12QX/sIVYpa0%3D&'} primaryText="2" />
                <MenuItem id="enpoint-select-3" value={'endpointrange?endpoint=ONltbQpsTErfDxfTm92syBU0EMg%3D&'} primaryText="3" />
                <MenuItem id="enpoint-select-4" value={'endpointrange?endpoint=nQSFLfSWb9yQt1o2qRRYE8Ct6IQ%3D&'} primaryText="4" />
                <MenuItem id="enpoint-select-5" value={'endpointrange?endpoint=iGE/C0ew1SRt6Qu0iP3O4nN3Qwc%3D&'} primaryText="5" />
                <MenuItem id="enpoint-select-6" value={'endpointrange?endpoint=TLYMdLOR9hZ13d4Uq/d0zevYNRg%3D&'} primaryText="6" />
                <MenuItem id="enpoint-select-7" value={'endpointrange?endpoint=cksXdpmFk+xWg19kNaaUeM/IAio%3D&'} primaryText="7" />
            </SelectField>
        );
    }
}
