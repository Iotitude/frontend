// libs
import React from 'react';
// components
import { MenuItem, SelectField } from 'material-ui';

export default class LimitType extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        }
        this.updateLimitType = this.updateLimitType.bind(this);
    }
    updateLimitType(e, index, value) {
        this.setState({value});
        this.props.updateLimitType(value);
    }
    /// !!! add <MenuItem /> with corresponding values if adding new limits
    render() {
        return(
            <SelectField
                id = "limit-select"
                floatingLabelText="Limit inputs"
                floatingLabelFixed={true}
                value={this.state.value}
                style={{maxWidth: '200px', marginLeft: '30px', marginRight: '30px'}}
                onChange={this.updateLimitType}>
                <MenuItem id="limit-select-none" value={''} primaryText="--" />
                <MenuItem id="limit-select-10" value={'&limit=10'} primaryText="10" />
                <MenuItem id="limit-select-50" value={'&limit=50'} primaryText="50" />
                <MenuItem id="limit-select-100" value={'&limit=100'} primaryText="100" />
                <MenuItem id="limit-select-1000" value={'&limit=1000'} primaryText="1 000" />
                <MenuItem id="limit-select-10000" value={'&limit=10000'} primaryText="10 000" />
                <MenuItem id="limit-select-50000" value={'&limit=50000'} primaryText="50 000" />
                <MenuItem id="limit-select-100000" value={'&limit=100000'} primaryText="100 000" />
            </SelectField>
        );
    }
}
