var config = {

    rest_api_url: process.env.REACT_APP_API_URL || 'localhost:8080/harmitus/'

}

module.exports = config;