# iotitude

Iotitude data visualization

Forked from [github](https://github.com/wimmalab/iotitude)

## Steps to run locally

1. [Install Node and make sure npm is also installed](https://nodejs.org/en/download/package-manager)
2. `git clone git@gitlab.com:Iotitude/frontend.git` or `git clone https://gitlab.com/Iotitude/frontend.git`
3. run `npm install` in project folder
4. run `npm start`

Components contain comments on how to modify / add certain options.
