# NodeJS version 8.1.3 and Alpine 3.6
FROM node:8.1.3-alpine
# Copy files and configure workdir
COPY ./ /usr/src/app
WORKDIR /usr/src/app
# install npm and start it
RUN npm install
CMD ["npm", "start"]